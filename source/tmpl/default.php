<?php
// No direct access
defined('_JEXEC') or die;


$Link = 'https://gratia-mira.ch/j3/?Layout=JSON&Stichwort=' . $params->get('filter');
$VersJSON = file_get_contents($Link);

// Umbrüche oder sonstige Zeichen löschen, die es aus irgendwelchen Gründen gibt
$VersObject = json_decode(preg_replace('/^.*\{/s','{',$VersJSON)); ?>

<style>
    .gratia-vers {
        background-color: #b6c8d9;
        padding: 15px;
        font-size: 30px;
    }
    .gratia-kommentar {
        background-color: lightgray;
        padding: 15px;
        font-size: 30px;
    }
    .kontext {
        opacity: 0.4;
    }
</style>

<div class="gratia-vers-beschreibung"><?php echo $params->get('beschreibung'); ?></div>
<div class="gratia-vers">
    <?php echo gvTools::formatiereSVG(urldecode($VersObject->VersFertig),'30'); ?>
</div>
<br>
<?php if($VersObject->KommentarFertig): ?>
    <div class="gratia-kommentar">
        <?php echo urldecode($VersObject->KommentarFertig); ?>
    </div>
<?php endif; ?>
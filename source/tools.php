<?php
// No direct access
defined('_JEXEC') or die;

class gvTools {

    // SVG formatieren (Höhe, Farbe, Style)
    public static function formatiereSVG($Konstante, $Hoehe = NULL, $Farbe = NULL, $Style = NULL) {
        
        // Höhe definieren
        if($Hoehe) {
            $RegexpHoehe = '/height=\'\d*\'/iU';
            $ErsetzenHohe = "height='" . $Hoehe . "'";
        } else {
            $RegexpHoehe = '//';
            $ErsetzenHohe = '';
        }

        // Farbe definieren
        if($Farbe) {
            $RegexpFarbe = '/fill=\'\w*\'/iU';
            $ErsetzenFarbe = "fill='" . $Farbe . "'";
        } else {
            $RegexpFarbe = '//';
            $ErsetzenFarbe = '';
        }

        // Style definieren
        if($Style) {
            $RegexpStyle = '/style=\'.*\'/iU';
            $ErsetzenStyle = "style='" . $Style . "'";
        } else {
            $RegexpStyle = '//';
            $ErsetzenStyle = '';
        }

        return preg_replace(array($RegexpHoehe,$RegexpFarbe,$RegexpStyle),array($ErsetzenHohe,$ErsetzenFarbe,$ErsetzenStyle),$Konstante);
    }
}
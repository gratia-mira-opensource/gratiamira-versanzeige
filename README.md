# Gratia Mira Versanzeige
Mit diesem Modul kann man auf einer beliebigen Joomla Seite einen Bibelvers anzeigen lassen.

# Basisfunktionen
Über den Administratorbereich können sowohl der Beschreibungstext als auch der Filter (das gesuchte Wort URL-Konform) konfiguriert werden.

# Erweitere Funktionen

## Weitere Filter
Grundsätzlich können alle Filter, bzw. Parameter, die auch auf Gratia-Mira verwendet werden eingegeben werden. Zuerst muss einfach das Stichwort kommen, danach gefolgt von einem `&` die weiteren Parameter. Interessant sind folgende Parameter:
* Bibelbuch={Bibelbuch} → Filtert nach einem Bibelbuch.
* Kommentare=Ja → Nur kommentierte Verse.
* JSON=Erweitert → Schaltet die Einzelwortsuche frei. Achtung, das Resultat wir nicht gefiltert. Dies kann auch zu schlechten Ergebnissen führen.

Bei weiteren Wünschen einfach ein Ticket auf GitLab erstellen.